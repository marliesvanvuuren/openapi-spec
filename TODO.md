# TODO

## Versioning

[x] trigger a new release with `npm version [<newversion> | major | minor | patch | premajor | preminor | prepatch | prerelease [--preid=<prerelease-id>] | from-git]`
[x] via gitlab-ci.yml: update version.yaml and index.html

## Splitting / sanitizing

[ ] split openapi | metadata | waardelijsten
[ ] remove $id / $schema
[ ] improve schema-names in bundle
[ ] validate json-schema with [ajv](https://ajv.js.org/)
