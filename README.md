# OpenAPI Specificatie

## Inleiding

Hier staat de OpenAPI Specificatie voor het aanleveren van documenten met bijbehorende metadata
om deze op PLOOI (*PLatform Open OverheidsInformatie*) openbaar te maken.
PLOOI is de aangewezen digitale infrastructuur waarbij bestuursorganen (overheidsorganisaties)
in het kader van de Woo (*Wet open overheid*) hun documenten centraal openbaar maken.

> # De API voor PLOOI - Aanleveren is nog sterk in ontwikkeling. Er kunnen uitdrukkelijk geen rechten worden ontleend aan deze repository.

Deze OpenAPI Specificatie beschrijft zowel de randvoorwaarden, het mechanisme en de inhoud van het
het aanleveren.

## Gebruik

Onder [fragmenten](./fragmenten) staan de fragmenten die samen de OpenAPI Specificatie vormen,
naast de benodigde code om deze fragmenten samen te voegen.

Op [GitLab Pages](https://koop.gitlab.io/plooi/aanleveren/openapi-spec/) staat deze informatie toegankelijk weergegeven.

### Lokaal bundelen en bekijken

Bij het doorvoeren van wijzigingen is het praktisch om lokaal (d.i. op de ontwikkelcomputer) de wijzigingen te kunnen bekijken.

#### Randvoorwaarden

1. een recente versie van [`nodejs`](https://nodejs.org) is lokaal geïnstalleerd
1. deze repository is lokaal uitgecheckt

#### Stappen

1. start de dev server: `npm run dev:start` De browser zal starten met de pagina vanaf `localhost:8080`
1. steeds wanneer de wijzigingen doorgevoerd moeten worden, vanuit een andere terminal:
    - `npm run dev:bundle` voor wijzigingen in OpenAPI fragmenten
    - `npm run dev:static` voor wijzigingen in Foutenbeschrijvingen
1. op het moment dat de bundel gemaakt is zal de door de vorige stap, zal de browser de tab verversen.

> **Opmerking**: in het lokaal-draaiende voorbeeld zullen, in plaats van de versienummers, de placeholders getoond worden.
> De rest van de pagina is een goede afspiegeling van hoe deze er uit komt te zien.

### Aanmaken van een release

Een release is een bundel van wijzigingen. Het is dus niet noodzakelijk om voor iedere separate wijziging een release
aan te maken.

#### Randvoorwaarden

1. een recente versie van [`nodejs`](https://nodejs.org) is lokaal geïnstalleerd
1. deze repository is lokaal uitgecheckt

#### Stappen

1. Commit alle code die in de release meegaat, voorzie iedere commit met een bericht in een gestandaardiseerd formaat:
    - `{type}({scope}): {onderwerp / samenvatting van de specifieke wijziging}, ADO #{work item nummer in KOOPs ADO}`
    - met voor `{type}` een van:
      - `build`: wijzigingen die het build systeem of externe afhankelijkheden betreffen
      - `ci`: wijzigingen aan CI configuratie en /of scripts
      - `docs`: wijzigingen beperkt tot alleen documentatie
      - `feat`: een nieuwe functionaliteit
      - `fix`: een reparatie van een bug
      - `perf`: een wijziging die de performance verbetert
      - `refactor`: een wijziging die geen bugs verhelpt en ook geen nieuwe functionaliteit toevoegt
      - `style`: wijzigingen die de betekenis van de code niet veranderen (witruimte, etc.)
      - `test`: toevoegen of aanpassen van testen
    - met voor `{scope}` het onderdeel waarin de wijziging is doorgevoerd

1. Markeer een nieuwe versie: `npm version {option}`
    - met voor `{option}` een van
      - `major`
      - `minor`
      - `patch`
      - letterlijke versie, bijvoorbeeld `0.1.1-alpha.2`, dus zonder voorloop-"v"
    - Dit past de `package.json` aan en maakt een `tag` aan

1. Push code naar GitLab.com: `git push`
1. Push tag naar GitLab.com: `git push origin {tag}`
    - met voor `{tag}` de zojuist aangemaakte tag
      - deze is op te vragen met `git tag` of door te kijken naar de uitvoer van de npm-version-stap hierboven
      - deze begint altijd met een voorloop-"v"

1. Monitor de pipeline
    - Navigeer in je browser naar [de pipelines op GitLab.com](https://gitlab.com/koop/plooi/aanleveren/openapi-spec/-/pipelines)
    - Controleer of de pipeline slaagt

1. Maak Release Notes
    - Navigeer in je browser naar [de tags op GitLab.com](https://gitlab.com/koop/plooi/aanleveren/openapi-spec/-/tags)
    - Klik op het potlood van de tag
    - Voer Release Notes in op basis van het volgende template:
<details><summary>template voor release notes (klik om uit te klappen)</summary>
```
#### _OpenAPI Specificatie PLOOI Aanleveren_

# Release notes, versie {tag}

## Over deze release
<details>

<summary>Klik om uit te klappen</summary>

Hier vindt u de OpenAPI Specificatie van de API-Aanleveren van PLOOI. Hierin wordt beschreven hoe documenten en bijbehorende metadata bij PLOOI geautomatiseerd kunnen worden aangeleverd en bijgewerkt.

In deze release {beschrijving van de release op hoofdlijnen}.

</details>

## Opgenomen in deze release
<details>

<summary>Klik om uit te klappen</summary>

### Features

* [{short-commit-sha}](https://gitlab.com/koop/plooi/aanleveren/openapi-spec/-/commit/{commit-sha}) {omschrijving van de commit},
* [{short-commit-sha}](https://gitlab.com/koop/plooi/aanleveren/openapi-spec/-/commit/{commit-sha}) {omschrijving van de commit},
{etc.}

OF

_geen wijzigingen_

### Fixes

* [{short-commit-sha}](https://gitlab.com/koop/plooi/aanleveren/openapi-spec/-/commit/{commit-sha}) {omschrijving van de commit},
* [{short-commit-sha}](https://gitlab.com/koop/plooi/aanleveren/openapi-spec/-/commit/{commit-sha}) {omschrijving van de commit},
{etc.}

OF

_geen wijzigingen_

### Documentatie

* [{short-commit-sha}](https://gitlab.com/koop/plooi/aanleveren/openapi-spec/-/commit/{commit-sha}) {omschrijving van de commit},
* [{short-commit-sha}](https://gitlab.com/koop/plooi/aanleveren/openapi-spec/-/commit/{commit-sha}) {omschrijving van de commit},
{etc.}

OF

_geen wijzigingen_

### Techniek

* [{short-commit-sha}](https://gitlab.com/koop/plooi/aanleveren/openapi-spec/-/commit/{commit-sha}) {omschrijving van de commit},
* [{short-commit-sha}](https://gitlab.com/koop/plooi/aanleveren/openapi-spec/-/commit/{commit-sha}) {omschrijving van de commit},
{etc.}

OF

_geen wijzigingen_

</details>
```

</details>

### Configureren welke releases worden gepubliceerd

#### Randvoorwaarden

1. een recente versie van [`nodejs`](https://nodejs.org) is lokaal geïnstalleerd
1. deze repository is lokaal uitgecheckt

#### Stappen

1. Open het bestand `.gitlab-ci.yaml`
1. Onder `variables`- bovenaan in het bestand - wijzig:
    - `HOME_TAG`: geef hier de versie(/tag), met prefix 'v', die op toplevel moet worden getoond (*voorbeeld: "v0.2.1-alpha"*)
    - `PUBLISH_TAGS`: geef hier een komma-gescheiden lijst van versies(/tags) die getoond moeten worden onder `/versies` (*voorbeeld: "v0.1.1-alpha.1, v0.1.1-alpha.2"*)
1. Commit en push het gewijzigde bestand `.gitlab-ci.yaml`

> **Opmerking:** onder `/versies` wordt naast de in `PUBLISH_TAGS` opgegeven versies ook steeds de laatste build(s) getoond
